const db = require('./modules/db')
const express = require('express')
const base64 = require('base-64')
const Hashids = require('hashids')
const app = express()
const port = 8000
const secret = 'App2ynTH@1234'
const padding = 6
const domain = 'http://localhost:8000'

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/short/get', async (req, res) => {
  let hash
  const url = req.query.url
  const base64Url = base64.encode(url)
  const hashids = new Hashids(secret, padding)
  const conn = await db.init()
  const row = await db.query(conn, `SELECT * FROM shorten_urls WHERE base64='${base64Url}' LIMIT 0,1`);
  if (row.length > 0) {
    hash = hashids.encode(row[0].id)
    res.set({
      'Content-Type': 'application/json'
    });
    res.send(JSON.stringify({
      id: hash,
      shorten_url: `${domain}/${hash}`
    }))
    return
  }

  const result = await db.query(conn, `INSERT INTO shorten_urls SET base64='${base64Url}'`);
  hash = hashids.encode(result.insertId)

  conn.end()

  res.set({
    'Content-Type': 'application/json'
  });
  res.send(JSON.stringify({
    id: hash,
    shorten_url: `${domain}/${hash}`
  }))
})

app.get('/:id', async (req, res) => {
  const hashids = new Hashids(secret, padding)
  const id = hashids.decode(req.params.id)
  const conn = await db.init()
  const result = await db.query(conn, `SELECT * FROM shorten_urls WHERE id=${id} LIMIT 0,1`);
  const url = base64.decode(result[0].base64)

  conn.end()

  res.redirect(url);
})


app.listen(port, () => console.log(`Example app listening on port ${port}!`))