const config = require('./config');
const mysql = require('mysql');

exports.init = async function () {
  const connection = mysql.createConnection(config.database);
  return new Promise((resolve) => {
    connection.connect((err) => {
      if (err) {
        resolve({ code: err.code });
      } else {
        resolve(connection);
      }
    });
  });
}

exports.query = async function (conn, sql) {
  return new Promise((resolve) => {
    conn.query(sql, function (err, result) {
      if (err) {
        return resolve([])
      }
      else {
        return resolve(result)
      }
    });
  });
}

exports.end = function (conn) {
  conn.end()
}